export let ProductService = {
  getListProduct: () => {
    return axios({
      url: "https://62b740320d4a2cd3e1a96fea.mockapi.io/QLSP",
      method: "GET",
    });
  },
  deleteProduct: (id) => {
    return axios({
      url: `https://62b740320d4a2cd3e1a96fea.mockapi.io/QLSP/${id}`,
      method: "DELETE",
    });
  },
  addProduct: (product) => {
    return axios({
      url: "https://62b740320d4a2cd3e1a96fea.mockapi.io/QLSP",
      method: "POST",
      data: product,
    });
  },
  getFoodById: (id) => {
    return axios({
      url: `https://62b740320d4a2cd3e1a96fea.mockapi.io/QLSP/${id}`,
      method: "GET",
    });
  },

  updateProduct: (id, product) => {
    return axios({
      url: `https://62b740320d4a2cd3e1a96fea.mockapi.io/QLSP/${id}`,
      method: "PUT",
      data: product,
    });
  },
};
