import { ProductService } from "../services/ProductServices.js";
import { products } from "../model/Products.js"
import { checkCharOnly, checkEmpty, checkMinMax, checkNumberOnly } from "../validation/validation.js";

let ProductList = [];
let renderProduct = (list) => {
    let contentHtml = "";
    for (let i = 0; i < list.length; i++) {
        let product = list[i];
        let contentCard = `<tr>
                    <td>${product.id}</td>
                    <td>${product.name}</td>
                    <td>${product.price}</td>
                    <td>${product.screen}</td>
                    <td>${product.backCamera}</td>
                    <td>${product.frontCamera}</td>
                    <td> <img src="${product.img}" alt=""></td>
                    <td>${product.desc}</td>
                    <td>${product.type}</td>
                    <td class="action"> 
                        <button class="btn btn-danger" onclick="deleteProduct(${product.id})" >Xoá</button>
                        <button class="btn btn-primary"data-toggle="modal" data-target="#myModal" onclick="loadToForm(${product.id})">Sửa</button>
                    </td>
                </tr>`;
        contentHtml = contentHtml + contentCard;
    }
    document.getElementById("tblListProduct").innerHTML = contentHtml;
};

let getProduct = () => {
    let id = document.getElementById("idProduct").value;
    let name = document.getElementById("name").value;
    let price = document.getElementById("price").value;
    let screen = document.getElementById("screen").value;
    let bcam = document.getElementById("bcam").value;
    let fcam = document.getElementById("fcam").value;
    let img = document.getElementById("img").value;
    let desc = document.getElementById("desc").value;
    let getType = document.getElementById("type");
    let type = getType.options[getType.selectedIndex].value;
    let product = new products(id, name, price, screen, bcam, fcam, img, desc, type);
    return product;
}

let checkExist = (nameProduct) => {
    let index = ProductList.findIndex((item) => {
        return item.name == nameProduct;
    });
    if (index == -1) {
        document.querySelector("#tbName").innerHTML = '';
        return true;
    } else {
        document.querySelector("#tbName").innerHTML = "Product already exists";
        return false;
    }
}

let checkProduct = (product) => {
    let flag = true;
    flag &= checkEmpty(product.name, '#tbName', 'Name') &
        checkEmpty(product.screen, '#tbScreen', 'Screen') &
        checkEmpty(product.backCamera, '#tbBcam', 'BackCamera') &
        checkEmpty(product.frontCamera, '#tbFcam', 'FrontCamera') &
        checkEmpty(product.img, '#tbImg', 'Image') &
        checkEmpty(product.desc, '#tbDesc', 'Description') &
        checkEmpty(product.type, '#tbType', 'Type');
    flag &= checkMinMax(product.price, '#tbPrice', 'Price', 1, 9999999);

    if (!flag) {
        return;
    }
    return flag;
}

let addProduct = () => {
    let product = getProduct();
    if (checkProduct(product)) {
        ProductService.addProduct(product)
            .then(() => {
                loadProduct();
            })
            .catch((error) => {

            });
    }
}

let deleteProduct = (id) => {
    ProductService.deleteProduct(id)
        .then(() => {
            loadProduct();
        })
        .catch((error) => {

        });
}

//  Update
let loadToForm = (id) => {
    document.getElementById("btnAdd").style.visibility = 'hidden';
    document.getElementById("btnUpDate").style.display = 'block';

    for (let i = 0; i < ProductList.length; i++) {
        let item = ProductList[i];
        if (id == item.id) {
            document.getElementById("idProduct").value = item.id;
            document.getElementById("name").value = item.name;
            document.getElementById("price").value = item.price;
            document.getElementById("screen").value = item.screen;
            document.getElementById("bcam").value = item.backCamera;
            document.getElementById("fcam").value = item.frontCamera;
            document.getElementById("img").value = item.img;
            document.getElementById("desc").value = item.desc;
            document.getElementById("type").value = item.type;

        }
    }
    let element = ['#tbName', '#tbScreen', '#tbPrice', '#tbBcam', '#tbFcam', '#tbImg', '#tbType', '#tbDesc']
    for (let i = 0; i < element.length; i++) {
        document.querySelector(element[i]).innerHTML = '';
    }

}

let clearForm = () => {
    document.getElementById("btnAdd").style.visibility = 'visible';
    document.getElementById("btnUpDate").style.display = 'none';
    document.getElementById("form").reset();
}

let updateProduct = () => {
    let product = getProduct();
    let id = document.getElementById("idProduct").value;
    if (checkProduct(product)) {
        ProductService.updateProduct(id, product)
            .then(() => {
                loadProduct();
            })
            .catch((error) => {

            });
    }
}




let loadProduct = () => {
    ProductService
        .getListProduct()
        .then((res) => {
            ProductList = res.data;
            renderProduct(ProductList);
        })
        .catch((err) => {
            console.log(err);
        });
};
loadProduct();
window.addProduct = addProduct;
window.loadToForm = loadToForm;
window.deleteProduct = deleteProduct;
window.clearForm = clearForm;
window.updateProduct = updateProduct;