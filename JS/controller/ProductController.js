import { ProductService } from "../services/ProductServices.js";
import { products } from "../model/Products.js";
var cart = [];

let ProductList = [];

let renderProduct = (listProducts) => {
    let contentHtml = "";
    for (let i = 0; i < listProducts.length; i++) {
        let product = listProducts[i];
        let contentCard = `<div class="thumb-wrapper slick-slide slick-active">
                                <div class="img-box">
                                    <img src="${product.img}"
                                        alt="">
                                </div>
                                <div class="thumb-content">
                                    <h4>${product.name}</h4>
                                    <p class="item-price"><span>$${product.price}</span></p>
                                    <div class="description">
                                        <p>${product.desc}</p>
                                    </div>                                                  
                                </div>
                                <div class="add-btn" id="${product.id}">
                                <a href="#" class="btn btn-primary" onclick={addToCart(${product.id})}>Add to Cart</a>
                            </div>
                            </div>`;
        contentHtml = contentHtml + contentCard;
    }
    document.getElementById("product").innerHTML = contentHtml;
};

let filterProduct = () => {
    let typePhone = [];
    let type = document.getElementById("select");
    let typeSelect = type.options[type.selectedIndex].value;
    for (let index = 0; index < ProductList.length; index++) {
        if (typeSelect == ProductList[index].type) {
            let phone = ProductList[index];
            typePhone.push(phone);
            renderProduct(typePhone);
        } else if (typeSelect == 0) {
            renderProduct(ProductList);
        }
    }
};
window.filterProduct = filterProduct;


let addToCart = (id) => {
    let cartItem = {
        products,
        quantity: 1,
    }
    let index = cart.findIndex((item) => {
        return item.product.id == id;
    })
    if (index == -1) {
        for (let i = 0; i < ProductList.length; i++) {
            if (id == ProductList[i].id) {
                cartItem.product = ProductList[i];
                cart.push(cartItem);
            }
        }
    } else {
        cart[index].quantity++;
    }
    numberTotal();
    numberItem();
}
window.addToCart = addToCart;

let numberTotal = () => {
    let total = 0;
    for (let i = 0; i < cart.length; i++) {
        total += cart[i].quantity * cart[i].product.price;
    }
    document.getElementById("total").innerHTML = "$" + total;
}

let numberItem = () => {
    let count = 0;
    for (let index = 0; index < cart.length; index++) {
        count += cart[index].quantity;
    }
    document.getElementById("number").innerText = count;
}


let deleteItem = (id) => {
    let index = cart.findIndex((item) => {
        return item.product.id == id;
    });
    cart.splice(index, 1);
    renderCart();
    numberTotal();
    numberItem();
}
window.deleteItem = deleteItem;

let changeQuantity = (id, step) => {
    let index = cart.findIndex((item) => {
        return item.product.id == id;
    });
    cart[index].quantity += step;
    if (cart[index].quantity == 0) {
        cart.splice(index, 1)
    }
    renderCart();
    numberTotal();
     numberItem();
}
window.changeQuantity = changeQuantity;

let order = () => {
    cart= [];
    alert("Order success");
    renderCart();
    numberTotal();
    numberItem();
}
window.order = order;

let renderCart = () => {
    let contentHtml = "";
    for (let i = 0; i < cart.length; i++) {
        let item = cart[i];
        let contentCart = `<tr>
                            <td data-th="Product">
                                <div class="row">
                                    <div class="col-sm-2 hidden-xs">
                                        <img src="${item.product.img}" alt="" class="img-responsive" />
                                    </div>
                                    <div class="col-sm-10">
                                        <h4 class="nomargin">${item.product.name}</h4>
                                        <p>${item.product.desc}</p>
                                    </div>
                                </div>
                            </td>
                            <td data-th="Price">$${item.product.price}</td>
                            <td data-th="Quantity" class="change_qty">
                                <button class="btn btn-danger btn-sm" onclick="changeQuantity(${item.product.id},${-1})">-</button>
                                <p class="text-center">${item.quantity}</p>                      
                                <button class="btn btn-success btn-sm" onclick="changeQuantity(${item.product.id},${1})">+</button>                        
                            </td>
                            <td data-th="Subtotal" class="text-center m-0">$${item.product.price * item.quantity}</td>
                            <td class="actions" data-th="">
                                <button class="btn btn-danger btn-sm" onclick="deleteItem(${item.product.id})"><i class="fa fa-trash-o"></i></button>
                            </td>
                        </tr>`;
        contentHtml += contentCart;
    }
    if (cart.length === 0) {
        document.getElementById("content-table").innerHTML = `<tr><td data-th="Price"><h5>Cart Is Empty</h5></td></tr>`;
    } else {
        document.getElementById("content-table").innerHTML = contentHtml;
    }

}
window.renderCart = renderCart;


let loadProduct = () => {
    ProductService
        .getListProduct()
        .then((res) => {
            ProductList = res.data;
            renderProduct(ProductList);
        })
        .catch((err) => {
            console.log(err);
        });
};
loadProduct();


